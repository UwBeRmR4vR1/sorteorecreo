﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Sorteo
{
    public partial class Sorteo : Form
    {
        #region DECLARACION DE VARIABLES
        string strres = "";
        DataSet 
            dsProceso = new DataSet(),
            listaTickets = new DataSet(),
            ganador = new DataSet();
        Stopwatch 
            contadorSorteo = new Stopwatch(),
            contadorDigito = new Stopwatch();
        List<string> 
            tickets = new List<string>(),
            listGanadores = new List<string>();
        Random random = new Random();
        List<PictureBox>
            pbdigito1 = new List<PictureBox>(),
            pbdigito2 = new List<PictureBox>(),
            pbdigito3 = new List<PictureBox>(),
            pbdigito4 = new List<PictureBox>(),
            pbdigito5 = new List<PictureBox>(),
            pbdigito6 = new List<PictureBox>(),
            pbdigito7 = new List<PictureBox>(),
            pbdigito8 = new List<PictureBox>();

        int movimientoY = 0, PosicionSiguiente, LongituFicha, PosicionEjeX = 3, posicion = 0, index = 0, digitos;
        int menor = 0, mayor = 0;
        bool I1 = false, I2 = false, I3 = false, I4 = false, I5 = false, I6 = false, I7 = false, I8 = false;
        public static string ticketGanador = "", telefonoGanador = "", nombreGanador = "", apellidoGanador = "", cedulaGanador = "";
        String nombreFicha1 = "", nombreFicha2 = "", nombreFicha3 = "", 
            nombreFicha4 = "", nombreFicha5 = "", nombreFicha6 = "",
            nombreFicha7 = "", nombreFicha8 = "", ticket = "";
        BDD BaseDeDatos = new BDD();
        #endregion
        public Sorteo()
        {
            InitializeComponent();
            timer1.Stop();
            timer2.Stop();
            timer3.Stop();
            timer4.Stop();
            timer5.Stop();
            timer6.Stop();
            timer7.Stop();
            timer8.Stop();
            //crearListaImagenes();
            CrearImagenes(pbdigito1, digito1, "Uno");
            CrearImagenes(pbdigito2, digito2, "Dos");
            CrearImagenes(pbdigito3, digito3, "Tres");
            CrearImagenes(pbdigito4, digito4, "Cuatro");
            CrearImagenes(pbdigito5, digito5, "Cinco");
            CrearImagenes(pbdigito6, digito6, "seis");
            CrearImagenes(pbdigito7, digito7, "siete");
            CrearImagenes(pbdigito8, digito8, "ocho");
        }
        /// <summary>
        /// Consulta la lista de empleados
        /// </summary>

        private void Sorteo_Load(object sender, EventArgs e)
        {
            digito8.Left = ((this.Width / 2) - ((4 * digito8.Width) + 70));
            digito7.Left = ((this.Width / 2) - ((3 * digito7.Width) + 50));
            digito6.Left = ((this.Width / 2) - ((2 * digito6.Width) + 30));
            digito5.Left = ((this.Width / 2) - (digito5.Width + 10));
            digito4.Left = ((this.Width / 2) + (10));
            digito3.Left = ((this.Width / 2) + ((1 * digito2.Width) + 30));
            digito2.Left = ((this.Width / 2) + ((2 * digito2.Width) + 50));
            digito1.Left = ((this.Width / 2) + ((3 * digito1.Width) + 70));

            digitos = 0;
            I1 = false; I2 = false; I3 = false; I4 = false; I5 = false; I6 = false; I7 = false; I8 = false;
            consultarTickets();
            llenaListaTicket();
            //Oculto los picturebox
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            //Fin 
            IniciaTimer();
            //Agrego el ganador del momento a las listas.
            listGanadores.Add(ticket);
        }
        private void consultarTickets()
        {
            Object[] objparam = new Object[1];
            objparam[0] = DateTime.Now.ToString("dd/MM/yyyy");
            listaTickets = BaseDeDatos.obtenerDatos("spObtieneTicketSorteo", objparam, ref strres);
        }
        /// <summary>
        /// Llena la lista de tickets validos
        /// </summary>
        private void llenaListaTicket(){
            /// <summary>
            /// Guarda el dataset en una lista para obtener el numero de ticket randomico
            /// </summary>
            //for (int i = 0; i <= listaTickets.Tables[0].Rows.Count - 1; i++)
            //{
            //    tickets.Add(listaTickets.Tables[0].Rows[i].ToString());
            //}
            menor = Convert.ToInt32(listaTickets.Tables[0].Rows[0].ItemArray[0].ToString());
            mayor = Convert.ToInt32(listaTickets.Tables[0].Rows[0].ItemArray[1].ToString());
        }
        /// <summary>
        /// Obtiene el ticket randomico
        /// </summary>
        private void obtieneTicketRandomico(){
            Random r = new Random();
            index = r.Next(menor,mayor);
            ticket = index.ToString();
            int ceros = 0;
            ceros = 8 - ticket.Length;
            for (int i = 0; i < ceros; i++)
            {
                ticket = '0' + ticket;
            }
        }
        /// <summary>
        /// funcion que asigna los digitos del boleto a los textbox
        /// </summary>
        private void AsignarDigitos(int digitos)
        {
            switch (digitos)
            {
                case 1:
                    I8 = true;
                    posicion = Convert.ToInt32(ticket.Substring(0, 1));
                    pictureBox8.Visible = true;
                    break;
                case 2:
                    I7 = true;
                    posicion = Convert.ToInt32(ticket.Substring(1, 1));
                    pictureBox7.Visible = true;
                    break;
                case 3:
                    I6 = true;
                    posicion = Convert.ToInt32(ticket.Substring(2,1));
                    pictureBox6.Visible = true;
                    break;
                case 4:
                    I5 = true;
                    posicion = Convert.ToInt32(ticket.Substring(3, 1));
                    pictureBox5.Visible = true;
                    break;
                case 5:
                    I4 = true;
                    posicion = Convert.ToInt32(ticket.Substring(4, 1));
                    pictureBox4.Visible = true;
                    break;
                case 6:
                    I3 = true;
                    posicion = Convert.ToInt32(ticket.Substring(5, 1));
                    pictureBox3.Visible = true;
                    break;
                case 7:
                    I2 = true;
                    posicion = Convert.ToInt32(ticket.Substring(6, 1));
                    pictureBox2.Visible = true;
                    break;
                case 8:
                    I1 = true;
                    posicion = Convert.ToInt32(ticket.Substring(7, 1));
                    pictureBox1.Visible = true;
                    break;
                default:
                    break;
            }
        }
        #region movimientoFicha
        public List<PictureBox> CrearImagenes(List<PictureBox> Lista, Panel panel, String nombreTag)
        {
            LongituFicha = 100;
            for (int i = 0; i < 9; i++)
            {
                PictureBox pb = new PictureBox();
                pb.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject("btnone" + (i + 1));
                pb.Size = new Size(LongituFicha, LongituFicha);
                pb.SizeMode = PictureBoxSizeMode.StretchImage;
                pb.Name = string.Format("{0}", i + nombreTag);
                pb.Tag = nombreTag + "_" + i;
                int Mov = pb.Location.Y;
                pb.Location = new Point(PosicionEjeX, i * LongituFicha);
                Lista.Add(pb);
                panel.Controls.Add(Lista[i]);
            }
            return Lista;
        }
        public String GirarFichas(List<PictureBox> Lista, Panel panel, Timer timer, bool bandera, int digito, PictureBox pb1)
        {
            for (int i = 0; i < Lista.Count; i++)
            {
                movimientoY = Lista[i].Location.Y;
                Lista[i].Location = new Point(PosicionEjeX, movimientoY - 10);
                if (Lista[i].Location.Y <= -LongituFicha)
                {
                    PosicionSiguiente = Lista[((Lista.Count) - 1)].Location.Y;
                    Lista[i].Location = new Point(PosicionEjeX, PosicionSiguiente + LongituFicha);
                    Lista.Add(Lista[i]);
                    panel.Controls.Add(Lista[i]);
                    Lista.RemoveAt(i);
                }
                if (bandera)
                {
                    if (Lista[i].Location.Y == (-10))
                    {
                        timer.Stop();
                        //panel.Visible = false;                        
                        pb1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject("btnone" + (posicion));
                        pb1.Size = new Size(LongituFicha, LongituFicha);
                        pb1.SizeMode = PictureBoxSizeMode.StretchImage;
                        pb1.Visible = true;
                    }

                }
            }            
            return "";
        }
        #endregion
        #region TIMERFICHAS
        /// <summary>
        /// Timer de las fichas.
        /// </summary>
        private void timer1_Tick(object sender, EventArgs e)
        {
            nombreFicha1 = GirarFichas(pbdigito1, digito1, timer1, I1,posicion, pictureBox1);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            nombreFicha2 = GirarFichas(pbdigito2, digito2, timer2, I2, posicion, pictureBox2);
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            nombreFicha3 = GirarFichas(pbdigito3, digito3, timer3, I3, posicion, pictureBox3);
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            nombreFicha4 = GirarFichas(pbdigito4, digito4, timer4, I4, posicion, pictureBox4);
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            nombreFicha5 = GirarFichas(pbdigito5, digito5, timer5, I5, posicion, pictureBox5);
        }

        private void timer6_Tick(object sender, EventArgs e)
        {
            nombreFicha6 = GirarFichas(pbdigito6, digito6, timer6, I6, posicion, pictureBox6);
        }
        private void timer7_Tick(object sender, EventArgs e)
        {
            nombreFicha7 = GirarFichas(pbdigito7, digito7, timer7, I7, posicion, pictureBox7);
        }

        private void timer8_Tick(object sender, EventArgs e)
        {
            nombreFicha8 = GirarFichas(pbdigito8, digito8, timer8, I8, posicion, pictureBox8);
        }
        #endregion
        #region TimerSorteo
        /// <summary>
        /// funcion que inicia el timer del sorteo
        /// </summary>
        private void IniciaTimer()
        {
            timerSorteo.Enabled = true;
            contadorSorteo.Start();
            //Inicia los timer que permiten el giro de los numeros.
            timer1.Start();
            timer2.Start();
            timer3.Start();
            timer4.Start();
            timer5.Start();
            timer6.Start();
            timer7.Start();
            timer8.Start();
        }
        /// <summary>
        /// funcion que termina el timer del sorteo
        /// </summary>
        private void TerminaTimer()
        {
            timerSorteo.Enabled = false; ;
            contadorSorteo.Restart();
            contadorSorteo.Stop();            
        }
        #endregion
        #region TimerDigito
        /// <summary>
        /// funcion que inicia el timer del sorteo
        /// </summary>
        private void IniciaTimerDigito()
        {
            timerDigito.Enabled = true;
            contadorDigito.Start();
        }
        /// <summary>
        /// funcion que termina el timer del sorteo
        /// </summary>
        private void TerminaTimerDigito()
        {
            timerDigito.Enabled = false; ;
            contadorDigito.Restart();
            contadorDigito.Stop();
        }
        #endregion
        #region logicaTimers
        /// <summary>
        /// Timer del sorteo.
        /// </summary>
        private void timerSorteo_Tick(object sender, EventArgs e)
        {
            obtieneTicketRandomico();
            if (contadorSorteo.Elapsed > TimeSpan.FromSeconds(5))
            {
                TerminaTimer();
                IniciaTimerDigito();
            }
        }
        /// <summary>
        /// Timer que detiene los numeros.
        /// </summary>
        private void timerDigito_Tick(object sender, EventArgs e)
        {
            if (digitos < 8)
            {
                digitos = digitos + 1;
                AsignarDigitos(digitos);
            }
            else
            {
                TerminaTimerDigito();

                Object[] objparam = new Object[2];
                objparam[0] = Convert.ToInt32(ticket);
                objparam[1] = DateTime.Now.ToString("dd/MM/yyyy");
                ganador = BaseDeDatos.obtenerDatos("spObtieneDatoGanadorSorteo", objparam, ref strres);

                infoGanador vistaGanador = new infoGanador();
                ticketGanador = ticket;
                telefonoGanador = ganador.Tables[0].Rows[0].ItemArray[0].ToString();
                nombreGanador = ganador.Tables[0].Rows[0].ItemArray[1].ToString();
                apellidoGanador = ganador.Tables[0].Rows[0].ItemArray[2].ToString();
                cedulaGanador = ganador.Tables[0].Rows[0].ItemArray[3].ToString();
                this.Close();
                vistaGanador.Show();
            }
        }
        #endregion
    }
}