﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sorteo
{
    public partial class Fondo : Form
    {
        int ganadores = 0;
        public Fondo()
        {
            InitializeComponent();
        }

        private void Fondo_Load(object sender, EventArgs e)
        {
            pictureBox1.Left = ((this.Width - pictureBox1.Width) / 2);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ganadores += 1;
            if (ganadores <= 10)
            {
                Sorteo sorteo = new Sorteo();
                sorteo.Show();
                lblIdGanador.Text = ganadores.ToString();
            }
            else
            {
                MessageBox.Show("El maximo numero de ganadores se alcanzo.", "Ganadores Maximos");
            }
        }
    }
}
