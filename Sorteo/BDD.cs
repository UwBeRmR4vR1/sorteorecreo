﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorteo
{
    /// <summary>
    /// Clase que permite la conexión a la base; setear datos(INSERT, UPDATE, DELETE) y Obtener datos (SELECT)
    /// </summary>
    class BDD
    {
        /// <summary>
        /// Objeto de tipo SIBDDNET propietaria SOLUINTE(Conexion a la BDD).
        /// </summary>
        private SIBDDNET.BDD objbdd = new SIBDDNET.BDD();
        /// <summary>
        /// Variable que se utiliza para conexión con el registro de windows.
        /// </summary>
        private string[] strparam;
        /// <summary>
        /// Variable utilizada para almacenar la conexión con la bdd.
        /// </summary>
        private object objcon;

        /// <summary>
        /// Método para extraer los datos de conexión del registro de windows
        /// </summary>
        /// <returns></returns>
        public bool SetearAplicacion()
        {
            try
            {
                string strres = "";

                objbdd.subRegistroBDD("SorteoSI", ref strparam, ref strres, "");
                objbdd.subConectarBDD(strparam, ref objcon, ref strres);
                if (strres != "c")
                {
                    objbdd.subDesconectarBDD(ref objcon, ref strres);
                    return false;
                }
                else
                {
                    objbdd.subDesconectarBDD(ref objcon, ref strres);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Constructor de la Clase
        /// </summary>
        public BDD()
        {
            SetearAplicacion();
        }

        /// <summary>
        /// Método para ejecutar un consulta a la base de datos
        /// </summary>
        /// <param name="nombreProcedimiento">Nombre del procedimiento a ejecutar</param>
        /// <param name="objparam">Parámetros de entrada del procedimiento almacenado</param>
        /// <param name="strres">Variable que retorna el estado de ejecución del procedimiento. En caso exitoso 'c'.</param>
        /// <returns></returns>
        public DataSet obtenerDatos(string nombreProcedimiento, Object[] objparam, ref string strres)
        {
            try
            {
                string strDesconectar = "";
                DataSet respuesta = new DataSet();
                objbdd.subConectarBDD(strparam, ref objcon, ref strres);
                respuesta = objbdd.GetDatosXStoredProcedureXParametros(nombreProcedimiento, objparam, ref objcon, ref strres);
                if (strres != "c")
                {
                    objbdd.subDesconectarBDD(ref objcon, ref strDesconectar);
                    return respuesta;
                }

                objbdd.subDesconectarBDD(ref objcon, ref strres);
                return respuesta;
            }
            catch (Exception ex)
            {
                strres = ex.Message;
                return null;
            }

        }

        /// <summary>
        /// Método para ejecutar un consulta a la base de datos 
        /// </summary>
        /// <param name="nombreProcedimiento">Nombre del procedimiento a ejecutar</param>
        /// <param name="strres">Variable que retorna el estado de ejecución del procedimiento. En caso exitoso 'c'.</param>
        /// <returns></returns>
        public DataSet obtenerDatos(string nombreProcedimiento, ref string strres)
        {
            try
            {
                string strDesconectar = "";
                DataSet respuesta = new DataSet();
                objbdd.subConectarBDD(strparam, ref objcon, ref strres);
                respuesta = objbdd.GetDatosXStoredProcedure(nombreProcedimiento, ref objcon, ref strres);
                if (strres != "c")
                {
                    objbdd.subDesconectarBDD(ref objcon, ref strDesconectar);
                    return respuesta;
                }

                objbdd.subDesconectarBDD(ref objcon, ref strres);
                return respuesta;
            }
            catch (Exception ex)
            {
                strres = ex.Message;
                return null;
            }

        }

        /// <summary>
        /// Método para ejecutar un procedimietno que no devuelve datos (INSERT, DELETE, UPDATE) a la base de datos 
        /// </summary>
        /// <param name="nombreProcedimiento">Nombre del procedimiento a ejecutar</param>
        /// <param name="strres">Variable que retorna el estado de ejecución del procedimiento. En caso exitoso 'c'.</param>
        /// <returns></returns>
        public bool setearDatos(string nombreProcedimiento, ref string strres)
        {
            try
            {
                string strDesconectar = "";
                objbdd.subConectarBDD(strparam, ref objcon, ref strres);
                objbdd.SetDatosXStoredProcedure(nombreProcedimiento, ref objcon, ref strres);
                if (strres != "c")
                {
                    objbdd.subDesconectarBDD(ref objcon, ref strDesconectar);
                    return false;
                }

                objbdd.subDesconectarBDD(ref objcon, ref strres);
                return true;
            }
            catch (Exception ex)
            {
                strres = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Método para ejecutar un procedimietno que no devuelve datos (INSERT, DELETE, UPDATE) a la base de datos
        /// </summary>
        /// <param name="nombreProcedimiento">Nombre del procedimiento a ejecutar</param>
        /// <param name="objparam">Parámetros de entrada del procedimiento almacenado</param>
        /// <param name="strres">Variable que retorna el estado de ejecución del procedimiento. En caso exitoso 'c'.</param>
        /// <returns></returns>
        public bool setearDatos(string nombreProcedimiento, Object[] objparam, ref string strres)
        {
            try
            {
                string strDesconectar = "";
                objbdd.subConectarBDD(strparam, ref objcon, ref strres);
                objbdd.SetDatosXStoredProcedureXParametros(nombreProcedimiento, objparam, ref objcon, ref strres);
                if (strres != "c")
                {
                    objbdd.subDesconectarBDD(ref objcon, ref strDesconectar);
                    return false;
                }

                objbdd.subDesconectarBDD(ref objcon, ref strres);
                return true;
            }
            catch (Exception ex)
            {
                strres = ex.Message;
                return false;
            }

        }

        /// <summary>
        /// Método para abrir conexión con la Base de Datos
        /// </summary>
        /// <returns>Devuelve un boleano que indica si la transaccion se ejecuto con éxito</returns>
        public bool Conectar()
        {
            string strres = "";
            objbdd.subConectarBDD(strparam, ref objcon, ref strres);
            if (strres != "c")
                return false;
            else
                return true;
        }

        /// <summary>
        /// Método para cerrar la conexión con la Base de Datos
        /// </summary>
        /// <returns>Devuelve un boleano que indica si la transaccion se ejecuto con éxito</returns>
        public bool Desconectar()
        {
            string strres = "";
            objbdd.subDesconectarBDD(ref objcon, ref strres);
            if (strres != "c")
                return false;
            else
                return true;
        }
    }
}
