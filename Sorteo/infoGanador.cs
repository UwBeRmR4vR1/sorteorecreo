﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sorteo
{
    public partial class infoGanador : Form
    {
        BDD bdd = new BDD();
        public PrintDocument PrintDoc;
        public infoGanador()
        {
            InitializeComponent();
            pictureBox1.Left = ((this.Width - pictureBox1.Width) / 2);
        }

        private void infoGanador_Load(object sender, EventArgs e)
        {
            //Asigno la data del txtUsuario
            txtUsuario.Text = Sorteo.apellidoGanador + ' ' + Sorteo.nombreGanador;
            txtUsuario.Left = ((this.Width - txtUsuario.Width) / 2);
            txtUsuario.Top = 80;
            //Asigno la data del txtTelefono
            txtTelefono.Text = "CI: " + Sorteo.cedulaGanador;
            txtTelefono.Left = ((this.Width - txtTelefono.Width) / 2);
            txtTelefono.Top = 80 + txtUsuario.Height;
            //Asigno la data del txtTicket
            txtTicket.Text = "Telefono: " + Sorteo.telefonoGanador;
            txtTicket.Left = ((this.Width - txtTicket.Width) / 2);
            txtTicket.Top = 80 + txtUsuario.Height + txtTelefono.Height + 10;
            //Asigno la data del txtFecha
            //txtFecha.Text = "";
            //txtFecha.Left = ((this.Width - txtFecha.Width) / 2);
            //txtFecha.Top = 80 + txtTicket.Height + txtTelefono.Height + txtTicket.Height + 30;
        }

        private void PrintDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Font LetraPequena = new Font("Arial", 8, FontStyle.Bold);
            Font LetraNormal = new Font("Arial", 10, FontStyle.Bold);
            Font LetraGrande = new Font("Impact", 20, FontStyle.Regular);
            int Inicio = 20;
            int Interlineado = 20;
            e.Graphics.DrawString("!FELICIDADES!", LetraGrande, Brushes.Black, 15, Inicio); Inicio += Interlineado + 25;
            e.Graphics.DrawString(Sorteo.apellidoGanador + ' ' + Sorteo.nombreGanador, LetraNormal, Brushes.Black, 15, Inicio); Inicio += Interlineado;
            e.Graphics.DrawString("CI: " + Sorteo.cedulaGanador, LetraNormal, Brushes.Black, 15, Inicio); Inicio += Interlineado;
            e.Graphics.DrawString("Telefono: " + Sorteo.telefonoGanador, LetraNormal, Brushes.Black, 15, Inicio); Inicio += Interlineado;
            e.Graphics.DrawString("Ticket: " + Sorteo.ticketGanador, LetraNormal, Brushes.Black, 15, Inicio); Inicio += Interlineado;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            PrintDialog PrintDlg;
            DialogResult result;

            try
            {
                // Inicializar el print dialog
                PrintDoc = new PrintDocument();
                PrintDlg = new PrintDialog();
                PrintDlg.Document = PrintDoc;
                // Mostrar el print dialog
                result = DialogResult.OK; // PrintDlg.ShowDialog()
                // Obtener el valor de vuelta del dialog
                if ((result == DialogResult.OK))
                {
                    PrintDoc.PrinterSettings = PrintDlg.PrinterSettings;
                    PrintDoc.DocumentName = "TicketGanador" + Sorteo.ticketGanador;
                    PrintDoc.PrintPage += new PrintPageEventHandler(this.PrintDoc_PrintPage);
                    PrintDoc.Print();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            PrintDoc = null;
            PrintDlg = null;
            this.Close();
        }
    }

}
